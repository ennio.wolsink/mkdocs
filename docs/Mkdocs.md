# MkDocs

This documentation can be generated into a rich HTML-powered and searchable website with [MkDocs](https://mkdocs.org).

## Installation

- Make sure you've following the project's [installation instructions](./Installation.md).
- Execute `docker compose up -d` in your terminal.

Now you can access the documentation at [http://localhost:8000](http://localhost:8000).

## Watching for changes

Whenever you make changes in `/docs` the documentation site gets re-built and the browser gets reloaded, as long as you keep the Docker container running. 

### Configuration

MkDocs is configured in `mkdocs.yml`. For a full reference of all the settings, read [the official MkDocs user guide on configuration](https://www.mkdocs.org/user-guide/configuration/).

### Excluded files/directories
Files and directories mentioned in the `plugins.exclude` key of `mkdocs.yml` are ignored ([reference](https://github.com/apenwarr/mkdocs-exclude)).

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[mkdocs]: http://www.mkdocs.org
[install]: http://www.mkdocs.org/#installation
[documentation]: http://www.mkdocs.org
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages