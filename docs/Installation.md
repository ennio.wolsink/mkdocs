# Installation

## Prerequisite

First download Docker for Desktop via [Docker](https://docker.com).

## Windows

Docker on Windows doesn't necessarily run properly right out the box. Follow [this guide from Microsoft](https://docs.microsoft.com/en-us/windows/wsl/install#step-4---download-the-linux-kernel-update-package) for detailed instructions. Also, you may need to install [this Linux kernel update package](https://docs.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package) to be able to start Docker.

## Mac OS

TODO

## Linux

### Debian/Ubuntu

TODO

### Redhat/CentOS

TODO

### Alpine

TODO
