# Writing tips

It's very likely many people will agree writing documentation is not really hard work. It is however more often than not subject of procrastination, leading many times to complete abandonment. This guide offers some tips to break through the writers block and get cracking at churning out some helpful words for yourself and your fellow persons.

## Just do it

Let's talk about something else first: doing chores. Things like doing the laundry, the dishes or cleaning out the litterbox. It's not some we enjoy, but it needs to be done anyway. If you don't do it, the primary function of your house, kitchen or litterbox won't suffer immediately. But eventually it'll all become a major mess, and trying to ignore it becomes near impossible.

Sounds familiar? Experienced developers know that writing software without writing documentation will work out fine in the short run. But in the long run, it'll be a mess.

So what's there to do about it? Just like a sensible approach to chores in the household, it's best to do them before the pile of work gets too big.

## Write documentation in small chunks

Take the first step by writing the first sentence of a new documentation article. The introduction of the introduction. The first noun. Just start moving your fingers over the keyboard. Then be proud of the fact you've been writing for a minute, then five and finally ten. If you quit at that point and decide it's now more important to reply to some e-mails or hash out some code, that's completely fine. In those ten minutes you can write more or less the same as everything that you've read in this guide so far. It goes faster than you think.

## Less is more

Documentation is not poetry. It's about transfering knowledge as efficiëntly as possible, meaning you can definitely make do with a minimal amount of effort. 

#### What Why Where When How

A helpful tool to use when writing concise technical texts is Why What Where When How (WWWWH). To break it down using an example, it goes a little something like this.

To help your team mates you've added a new job to be executed automatically  when code gets pushed to the Git repository on GitLab, to generate translation files based on metadata in the HTML templates of an Angular app. Now you need to let your team know about it. 

To push through the writers block, you can ask yourself the WWWWH-questions and answer them while writing out the documentation.

- Why?
    - A script to generate translation files based on metata in the HTML templates of an Angular app needs to be run manually, which is often forgotten. This leads to unexpected compile time errors, failed deloyments and frustration all around.
- What is it?
    - A GitLab CI job to automatically generate the aforementioned translation files and solve the problem of forgotten manual runs. The translation files are stored in an AWS Simple Storage Service (S3) bucket for later download by the app right before it gets compiled. To make this happen, the existing build script `npm run build` has been modified in `package.json`. The bucket name is defined in a Gitlab CI variable called `TRANSLATIONS_S3_BUCKET`.
- Where?
    - The GitLab project Awesome Cat Videos holds the CI pipeline job we just added.
- When?
    - Once Git commits with code changes are pushed with Git to the GitLab repository.
- How?
    - The script which has been used locally is now being called in the CI job, inside a Docker container based off a recent Node.js public Docker image, while all required npm dependencies have been installed earlier in the pipeline and cached.

### Putting it all together

Once the questions What? Why? Where? When? and How? are answered (which you can do mentally inside your head or by scribbling some keywords on paper to save time), you can combine the information these answers have yieled into a few coherent yet concise sentences:

> A script to generate translation files for the Awesome Cat Videos Angular app is available in the GitLab CI pipeline. It'll run automatically once commits are pushed to the repository and uploads the generated to an AWS S3 bucket. These translations files are automatically downloaded right before compiling the app when running `npm run build`. The bucket name is configured in the GitLab CI variable `TRANSLATIONS_S3_BUCKET`. To change it, go to Settings > CI/CD / Variables.

Easy peazy.

## POS: Polish, Overturn, Scratch (Kill Your Darlings)

It's kind of an illusion that you can write a good text in a single attempt. Writing is like sculpting: you start with a raw form which you keep *Polishing* afterwards, so a sentence you wrote gets changed a few times. Upon re-reading your text a few times, you decide to completely *Overturn* how you formulated a few sentences. Basically rewriting it. After another few re-reads, you decide to *Scratch* a few sentences, killing your precisously crafted darlings by deleting them. The end result is a clear text which makes sense to most people.

!!! Tip
    The process of POS and Kill Your Darlings usually just happens naturally if you adopt the habit of re-reading your texts over and over again (within reason of course).

## Use lots of (sub) headings

People often don't read entire texts, especially if it's just a wall of words. To make it easier for them, break up your text in clear sub-topics and give them appropriate titles. Pay careful attention to subsub-topics and even subsubsub-topics or deeper and give them the right heading level, [which is easy to do in Markdown](https://www.markdownguide.org/basic-syntax/#headings). When using MkDocs, this leads to a very helpful automatically generated table of contents in the right sidebar.

## Link to appropriate references and sources

Is there some article or existing piece of documentation somewhere which explains what you want to document more than good enough? Save yourself the trouble and simply link to it. All you'd have to do then is provide some context, like:

> To familiarize yourself with the best practices when writing Cypress tests, check out [the relevant guide in the official Cypress documentation](https://docs.cypress.io/guides/references/best-practices).

You can also use the Material theme's footnotes and be even more concise:

```text
Don't forget to apply best practices when writing Cypress tests[^1].

[^1]: [Best Practices | Cypress Documentation](https://docs.cypress.io/guides/references/best-practices)
```

Which looks like this:

Don't forget to apply best practices when writing Cypress tests[^1].

!!! Tip
    Scroll down to the bottom of this guide to actually see the footnote.

[^1]: [Best Practices | Cypress Documentation](https://docs.cypress.io/guides/references/best-practices)

## Supplement text with rich media

Good documentation isn't just all words. With the [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) theme things get easy. When appropriate, add [images](https://squidfunk.github.io/mkdocs-material/reference/images/#image-captions), [diagrams](https://squidfunk.github.io/mkdocs-material/reference/diagrams/), [footnotes](https://squidfunk.github.io/mkdocs-material/reference/footnotes/), [admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/) (aka "call outs"), [code blocks](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/), etc.

## Some ideas for topics to write about

- Detailed information about development, testing and acceptance (DTA) servers for a given application or group of applications
- How to setup a local development environment
- Onboarding new team members
- Step by step instructions for configuring a specific tool, library or framework
- DevOps in practice: how CI pipelines are setup for your team and how to operate them
- Definition of Done/Definition of Ready
- Architetural guidelines
- Code review checklist
- Monitoring tools used by the team
- Location of the team drive with all sorts of documents