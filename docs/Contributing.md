# Contributing

Want to help expand on this project by writing some tidbits of documentation yourself? Great!

## Getting started

First, check out [the installation instructions](/Installation) for instructions on how to run MkDocs locally using [Docker](https://docker.com).

## Fork this project

Go to the [projects mainpage](https://gitlab.com/ennio.wolsink/mkdocs) and click on the fork button at the right top.

## Add/change some texts

Use [Markdown syntax](https://www.markdownguide.org/) to write some new or changed texts in the `/docs` directory.

## Submit a merge request 

Go to the [projects mainpage](https://gitlab.com/ennio.wolsink/mkdocs). Select *Merge requests* in the left sidebar and click on the button *New merge request*. In the following screen, select as *Source branch* the branch in your own fork. As *Target branch* choose **Master** of the repository `ennio.wolsink/mkdocs`.