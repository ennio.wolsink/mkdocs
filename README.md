![Build Status](https://gitlab.com/ennio.wolsink/mkdocs/badges/master/build.svg)

# MkDocs

Example [MkDocs](https://mkdocs.org) website using GitLab Pages. MkDocs is open source software to generate a rich HTML documentation site based off Markdown files.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

## Contributing

Check out [the Contribution guide](http://ennio.wolsink.gitlab.io/mkdocs/Contributing.md) for instructions on how to proceed.

---

Forked from https://gitlab.com/pages/mkdocs
